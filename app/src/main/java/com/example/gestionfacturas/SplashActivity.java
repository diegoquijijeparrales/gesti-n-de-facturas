package com.example.gestionfacturas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.example.gestionfacturas.ui.login.LoginActivity;


public class SplashActivity extends AppCompatActivity {

    static int TIMEOUT_MILLIS = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent es la definición abstracta de una operación a realizar.
                //Puede ser usado para una activity, broadcast receiver, servicios.
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                //Finaliza esta activity
                finish();
            }
        }, TIMEOUT_MILLIS);

    }

}
